import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/test",
      name: "Test",
      component: () => import("./views/Test.vue"),
      meta: {}
    },
    {
      path: "/test/:username",
      name: "Test3",
      component: () => import("./views/Test2.vue"),
      meta: {}
    },
    {
      path: "/test2",
      name: "Test2",
      component: () => import("./views/Test2.vue"),
      meta: {}
    },
    {
      path: "/",
      name: "Home",
      component: () => import("./views/Home.vue"),
      meta: {
        guest: true
      }
    },
    {
      path: "/dashboard",
      name: "Dashboard",
      component: () => import("./views/Dashboard.vue"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/registermikamolly",
      name: "Register",
      component: () => import("./views/Register.vue"),
      meta: {
        guest: true
      }
    },
    {
      path: "/loginmikamolly",
      name: "Login",
      component: () => import("./views/Login.vue"),
      meta: {
        guest: true
      }
    },
    {
      path: "/s/:slug",
      name: "Site",
      component: () => import("./views/Site.vue")
    },
    {
      path: "/editor/:slug",
      name: "Editor",
      component: () => import("./views/Editor.vue"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/demo/:slug",
      name: "Demo",
      component: () => import("./views/Demo.vue")
    },
    {
      path: "/about",
      name: "About",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("./views/About.vue")
    },
    {
      path: "/connect",
      name: "Connect",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("./views/Connect.vue")
    }
  ]
});

router.beforeEach((to, from, next) => {
  console.log("BEFORE");
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem("jwtToken") == null) {
      next({
        path: "/",
        params: { nextUrl: to.fullPath }
      });
    } else {
      next();
      let user = JSON.parse(localStorage.getItem("user"));
      if (to.matched.some(record => record.meta.is_admin)) {
        if (user.is_admin == 1) {
          next();
        } else {
          next({ name: "Dashboard" });
        }
      } else {
        next();
      }
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem("jwtToken") == null) {
      next();
    } else {
      next({ name: "Dashboard" });
    }
  } else {
    next();
  }
});

export default router;
