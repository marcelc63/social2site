import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    list: [],
    search: "",
    meta: {
      id: "",
      slug: "",
      image: ""
    },
    site: {
      author: "",
      background: "color",
      header: "center",
      headerBackground: "color",
      headerWidth: "7/12",
      bodyWidth: "7/12",
      card: "row",
      cardCol: "3",
      title: "Social2Site",
      tagline: "My list of products to share...",
      searchPlaceholder: "Search..."
    },
    style: {
      backgroundColor: "#e6ecf0",
      backgroundImage: "",
      headerColor: "#e6ecf0",
      headerImage: "",
      headerRounded: "rounded-none",
      headerMarginTop: "mt-0",
      headerOverlay: "rgba(0, 0, 0, 0.3)",
      bodyColor: "#e6ecf0",
      titleColor: "#000000",
      taglineColor: "#000000",
      cardColor: "#ffffff",
      cardRounded: "rounded",
      cardPrimaryColor: "#2c3e50",
      cardSecondaryColor: "#718096",
      cardButtonColor: "#edf2f7",
      cardButtonTextColor: "#718096",
      searchColor: "#ffffff",
      searchTextColor: "#2c3e50",
      searchButtonColor: "#a0aec0",
      searchButtonTextColor: "#ffffff"
    }
  },
  mutations: {
    change(state, payload) {
      let { key, data } = payload;
      state[key] = data;
    },
    site(state, payload) {
      let { key, data } = payload;
      state.site[key] = data;
    },
    style(state, payload) {
      let { key, data } = payload;
      state.style[key] = data;
    },
    meta(state, payload) {
      let { key, data } = payload;
      state.meta[key] = data;
    }
  },
  actions: {
    change({ commit }, payload) {
      commit("change", payload);
    },
    site({ commit }, payload) {
      commit("site", payload);
    },
    style({ commit }, payload) {
      commit("style", payload);
    },
    meta({ commit }, payload) {
      commit("meta", payload);
    }
  },
  getters: {
    state(state) {
      return state;
    },
    site(state) {
      return state.site;
    },
    style(state) {
      return state.style;
    },
    meta(state) {
      return state.meta;
    },
    list(state) {
      if (state.search !== "") {
        let search = state.search.split(" ");

        function queryCheck(query) {
          let check = true;
          search.forEach(x => {
            if (check === true) {
              check = query.name.toLowerCase().includes(x.toLowerCase());
            }
          });
          return check;
        }

        let final = state.list.filter(x => {
          return queryCheck(x);
        });
        return final;
      }
      return state.list;
    },
    search(state) {
      return state.search;
    }
  }
});
