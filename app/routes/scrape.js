var express = require("express");
var router = express.Router();
var getUser = require("../scraper/instagram/index.js");

/* GET ALL */
router.get("/:username", async function(req, res) {
  let social = await getUser(req.params.username);
  console.log(social);
  res.send(social);
});

module.exports = router;
