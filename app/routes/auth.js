var mongoose = require("mongoose");
var passport = require("passport");
var settings = require("../config/settings");
require("../config/passport")(passport);
var express = require("express");
var jwt = require("jsonwebtoken");
var router = express.Router();
var User = require("../models/User");

router.post("/register", function(req, res) {
  console.log("hi", req.body);
  if (!req.body.username || !req.body.password) {
    console.log("hi2");
    res.json({ success: false, msg: "Please pass username and password." });
  } else {
    var newUser = new User({
      username: req.body.username,
      password: req.body.password,
      code: "358f502059ef90f3cbfcf9383dc3b232d58f9f37bb46c0895d321cc98398226f"
    });
    // save the user
    newUser.save(function(err) {
      console.log("hi3");
      if (err) {
        return res.json({ success: false, msg: "Username already exists." });
      }
      console.log("hi4");
      res.json({ success: true, msg: "Successful created new user." });
    });
  }
});

router.post("/login", function(req, res) {
  User.findOne(
    {
      username: req.body.username
    },
    function(err, user) {
      if (err) throw err;

      if (!user) {
        res.status(401).send({
          success: false,
          msg: "Authentication failed. User not found."
        });
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var token = jwt.sign(user.toJSON(), settings.secret);
            // return the information including token as JSON
            res.json({ success: true, token: "JWT " + token, code: user.code });
          } else {
            res.status(401).send({
              success: false,
              msg: "Authentication failed. Wrong password."
            });
          }
        });
      }
    }
  );
});

router.post("/logout", function(req, res) {
  req.logout();
  res.json({ success: true });
});

module.exports = router;
