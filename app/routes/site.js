var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Site = require("../models/Site.js");
var passport = require("passport");
require("../config/passport")(passport);

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

/* GET ALL */
router.get("/", passport.authenticate("jwt", { session: false }), function(
  req,
  res
) {
  console.log(req.user);
  var token = getToken(req.headers);
  if (token) {
    Site.find({ owner: req.user._id }, function(err, sites) {
      if (err) return next(err);
      res.json(sites);
    });
  } else {
    return res.status(403).send({ success: false, msg: "Unauthorized." });
  }
});

/* GET ONE */
router.get("/:slug", function(req, res) {
  Site.find({ slug: req.params.slug }, function(err, sites) {
    if (err) return next(err);
    res.json(sites);
  });
});

/* SAVE */
router.post("/", passport.authenticate("jwt", { session: false }), function(
  req,
  res
) {
  var token = getToken(req.headers);
  if (token) {
    let payload = {
      ...req.body,
      content: JSON.stringify(req.body.content),
      owner: req.user._id
    };
    // console.log(payload);
    // return res.json(403);
    Site.create(payload, function(err, post) {
      if (err) return console.log(err);
      // console.log("done");
      res.json(post);
    });
  } else {
    return res.status(403).send({ success: false, msg: "Unauthorized." });
  }
});

/* UPDATE */
router.post(
  "/update/:id",
  passport.authenticate("jwt", { session: false }),
  function(req, res, next) {
    // console.log("update");
    var token = getToken(req.headers);
    if (token) {
      Site.findOneAndUpdate({ _id: req.params.id }, req.body, function(
        err,
        post
      ) {
        if (err) return next(err);
        res.json(post);
      });
    } else {
      return res.status(403).send({ success: false, msg: "Unauthorized." });
    }
  }
);

/* DELETE */
router.post(
  "/delete/:id",
  passport.authenticate("jwt", { session: false }),
  function(req, res) {
    // console.log("delete");
    var token = getToken(req.headers);
    if (token) {
      Site.findByIdAndRemove(req.params.id, function(err, post) {
        if (err) return console.log(err);
        console.log("done");
        res.json(post);
      });
    } else {
      return res.status(403).send({ success: false, msg: "Unauthorized." });
    }
  }
);

module.exports = router;
