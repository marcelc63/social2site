var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var User = require("../models/User.js");
var passport = require("passport");
require("../config/passport")(passport);
var jwt = require("jsonwebtoken");
var settings = require("../config/settings");
let axios = require("axios");
require("../config/passport")(passport);

let salt = "mikamolly";

var { createApolloFetch } = require("apollo-fetch");
const fetch = createApolloFetch({
  uri: "https://api.producthunt.com/v2/api/graphql"
});

/* GET ALL */
router.get("/", function(req, res) {
  let code = req.query.code;
  // console.log("hi");
  let payload = {
    client_id:
      "4a0b0515ac8fee8c5a1e32ded60b7864a76d4dc4adf73ef8c45a601b1c9d22e0",
    client_secret:
      "aed42106faffc228915f60fd2913b6444f59e0bcba37caaacf8d9f0d6d116c90",
    redirect_uri: "https://social2site.com/api/connect",
    code: code,
    grant_type: "authorization_code"
  };
  // console.log(payload);
  axios
    .post("https://api.producthunt.com/v2/oauth/token", payload)
    .then(result => {
      // console.log(result);
      let access_token = result.data.access_token;
      getPHUser(req, res, access_token);
    })
    .catch(e => {
      console.log("error", e);
    });
  // res.send("hi");
  // res.redirect(`/connect/?code=${code}`);
});

router.post("/login", function(req, res) {
  login(req, res);
});

function getPHUser(req, res, access_token) {
  fetch.use(({ request, options }, next) => {
    if (!options.headers) {
      options.headers = {}; // Create the headers object if needed.
    }
    options.headers["Authorization"] = "Bearer " + access_token;

    next();
  });
  fetch({
    query: `query { 
      viewer { user { 
        id 
        username
        name
      }
    }}`
  }).then(result => {
    // console.log("PH Data", result.data);
    let data = result.data.viewer.user;
    let payload = {
      username: data.username,
      password: salt + data.username,
      name: data.name,
      access_token: access_token
    };
    login(req, res, payload);
  });
}

function register(req, res, payload) {
  // console.log("resiget payload", payload);
  var newUser = new User({
    username: payload.username,
    password: payload.password,
    code: payload.access_token,
    name: payload.name
  });
  // save the user
  newUser.save(function(err) {
    if (err) {
      console.log(err);
      return res.json({ success: false, msg: "Username already exists." });
    }
    // res.json({ success: true, msg: "Successful created new user." });
    login(req, res, payload);
  });
}

function login(req, res, payload) {
  User.findOne(
    {
      username: payload.username
    },
    function(err, user) {
      if (err) throw err;

      if (!user) {
        // res.status(401).send({
        //   success: false,
        //   msg: "Authentication failed. User not found."
        // });
        register(req, res, payload);
      } else {
        // check if password matches
        user.comparePassword(payload.password, function(err, isMatch) {
          if (isMatch && !err) {
            var token = jwt.sign(user.toJSON(), settings.secret);
            user.code = payload.access_token;
            user.save();
            let newToken = "JWT " + token;
            // res.json({ success: true, token: newToken, code: user.code });
            res.redirect("/connect?token=" + newToken + "&code=" + user.code);
          } else {
            res.status(401).send({
              success: false,
              msg: "Authentication failed. Wrong password."
            });
          }
        });
      }
    }
  );
}

module.exports = router;
