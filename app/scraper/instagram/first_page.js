let axios = require("axios");
var md5 = require("md5-hex");
let INSTAGRAM_URL = "https://www.instagram.com";

async function getFirstPage(data) {
  var profileURL = data.match("/static(.*)ProfilePageContainer.js/(.*).js")[0];
  try {
    const response = await axios.get(`${INSTAGRAM_URL}${profileURL}`);
    let body = response.data;
    if (body) {
      let queryID = body.match(
        'profilePosts.byUserId.get([^"]*)queryId:"([^"]*)"'
      )[2];
      return queryID;
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = getFirstPage;
