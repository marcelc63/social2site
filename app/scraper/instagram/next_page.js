let axios = require("axios");
var md5 = require("md5-hex");
let INSTAGRAM_URL = "https://www.instagram.com";

async function getNextPage(payload) {
  let {
    profile_id,
    csrf,
    post_per_req,
    end_cursor,
    rhx_gis,
    query_id
  } = payload;
  let params =
    '{"id":"' +
    profile_id +
    '","first":' +
    post_per_req +
    ',"after":"' +
    end_cursor +
    '"}';
  let ig_gis = md5(rhx_gis + ":" + params);
  let config = {
    headers: {
      "User-Agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0",
      "x-instagram-gis": ig_gis
    }
  };
  let url =
    INSTAGRAM_URL +
    "/graphql/query/?query_hash=" +
    query_id +
    "&variables=" +
    params;
  try {
    const response = await axios.get(url, {}, config);
    let body = response.data;
    if (body) {
      // let end_cursor
      let media = body.data.user.edge_owner_to_timeline_media;
      let page_info = media.page_info;
      console.log(page_info);
      let has_next_page = page_info.has_next_page;
      let end_cursor = page_info.end_cursor;
      let photos = media.edges;
      let count = media.count;

      let limit = false; //For limiting amount taken

      if (has_next_page && limit) {
        let additions = await getNextPage({
          ...payload,
          end_cursor
        });
        return {
          count,
          photos: photos.concat(additions.photos)
        };
      } else {
        return {
          count,
          photos
        };
      }
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = getNextPage;
