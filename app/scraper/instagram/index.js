let axios = require("axios");
var md5 = require("md5-hex");
const fs = require("fs");
let INSTAGRAM_URL = "https://www.instagram.com";
let getFirstPage = require("./first_page.js");
let getNextPage = require("./next_page.js");

async function getUser(user) {
  try {
    const response = await axios.get(`${INSTAGRAM_URL}/${user}`);
    let body = response.data;
    if (body) {
      var data = body.match("window._sharedData = ([^]*)};</script>");
      data = data
        ? JSON.parse(
            body.match("window._sharedData = ([^]*)};</script>")[1] + "}"
          )
        : false;
      let queryID = await getFirstPage(body);
      //   Define
      let profilePage = data.entry_data.ProfilePage[0];
      let payload = {
        profile_id: profilePage.graphql.user.id,
        csrf: data.config.csrf_token,
        post_per_req: 50,
        end_cursor: "",
        rhx_gis: "",
        query_id: queryID
      };
      let scraped = await getNextPage(payload);
      console.log(profilePage);
      console.log(queryID);
      console.log(scraped);
      console.log(scraped.photos.length);
      let toSave = {
        profile: {
          biography: profilePage.graphql.user.biography,
          full_name: profilePage.graphql.user.full_name,
          username: profilePage.graphql.user.username,
          profile_pic_url_hd: profilePage.graphql.user.profile_pic_url_hd
        },
        photos: scraped.photos.map(x => {
          let res = x.node;
          return {
            id: res.id,
            display_url: res.display_url,
            shortcode: res.shortcode,
            dimensions: res.dimensions,
            thumbnail_src: res.thumbnail_src,
            thumbnail_resources: res.thumbnail_resources.reverse(),
            caption: res.edge_media_to_caption.edges[0].node.text
          };
        })
      };
      // fs.writeFile("./data.json", JSON.stringify(toSave), function(err) {
      //   if (err) {
      //     return console.log(err);
      //   }

      //   console.log("The file was saved!");
      // });

      // let saveRaw = {
      //   profilePage,
      //   scraped
      // };
      // fs.writeFile("./raw.json", JSON.stringify(saveRaw), function(err) {
      //   if (err) {
      //     return console.log(err);
      //   }

      //   console.log("The file was saved!");
      // });
      return toSave;
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = getUser;
