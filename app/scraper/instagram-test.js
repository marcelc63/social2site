let axios = require("axios");
var md5 = require("md5-hex");
let INSTAGRAM_URL = "https://www.instagram.com";
let allPhoto = [];

function get_page(
  profile_id,
  csrf,
  post_per_req,
  end_cursor,
  rhx_gis,
  query_id
) {
  let params =
    '{"id":"' +
    profile_id +
    '","first":' +
    post_per_req +
    ',"after":"' +
    end_cursor +
    '"}';
  var ig_gis = md5(rhx_gis + ":" + params);
  let config = {
    headers: {
      "User-Agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0",
      "x-instagram-gis": ig_gis
    }
  };

  var url =
    INSTAGRAM_URL +
    "/graphql/query/?query_hash=" +
    query_id +
    "&variables=" +
    params;

  console.log(csrf, params, config, url, ig_gis);

  axios
    .get(url, {}, config)
    .then(res => {
      console.log(res.data);
      let body = res.data;
      let photos = body.data.user.edge_owner_to_timeline_media;
      console.log(photos.edges);
      allPhoto = allPhoto.concat(photos.edges);
      console.log("LENGTH", allPhoto.length);
      if (photos.page_info.has_next_page && allPhoto.length <= 200) {
        get_page(profile_id, csrf, post_per_req, end_cursor, rhx_gis, query_id);
      }
    })
    .catch(e => {
      console.log(e);
    });
}

axios
  .get(INSTAGRAM_URL + "/aliciayudos")
  .then(function(response) {
    // handle success
    let body = response.data;
    if (body) {
      var _sharedDataStr = body.match("window._sharedData = ([^]*)};</script>");
      _sharedData = _sharedDataStr
        ? JSON.parse(
            body.match("window._sharedData = ([^]*)};</script>")[1] + "}"
          )
        : false;

      // console.log(_sharedData);
      let profilePage = _sharedData.entry_data.ProfilePage[0];
      let profile_id = profilePage.graphql.user.id;
      let csrf = _sharedData.config.csrf_token;
      let post_per_req = 50;
      let end_cursor = "";
      let rhx_gis = "";

      var ProfilePageContainerJS = body.match(
        "/static(.*)ProfilePageContainer.js/(.*).js"
      )[0];
      console.log(ProfilePageContainerJS);
      axios
        .get(INSTAGRAM_URL + ProfilePageContainerJS)
        .then(response => {
          let body = response.data;
          // console.log(body);
          var query_id = body.match(
            'profilePosts.byUserId.get([^"]*)queryId:"([^"]*)"'
          )[2];
          console.log(query_id);

          get_page(
            profile_id,
            csrf,
            post_per_req,
            end_cursor,
            rhx_gis,
            query_id
          );

          return;
          let params =
            '{"id":"' +
            profile_id +
            '","first":' +
            post_per_req +
            ',"after":"' +
            end_cursor +
            '"}';
          var ig_gis = md5(rhx_gis + ":" + params);
          let config = {
            headers: {
              "User-Agent":
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0",
              "x-instagram-gis": ig_gis
            }
          };

          var url =
            INSTAGRAM_URL +
            "/graphql/query/?query_hash=" +
            query_id +
            "&variables=" +
            params;

          console.log(csrf, params, config, url, ig_gis);

          axios
            .get(url, {}, config)
            .then(res => {
              console.log(res.data);
              let body = res.data;
              let photos = body.data.user.edge_owner_to_timeline_media;
              console.log(photos);
            })
            .catch(e => {
              console.log(e);
            });
        })
        .catch(e => {
          console.log(e);
        });
    }
  })
  .catch(function(error) {
    // handle error
    console.log(error);
  })
  .finally(function() {
    // always executed
  });
