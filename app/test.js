// Builder Structure Testing
// Inspired by Flutter

let page = {
  meta: {
    // Meta stuff
  },
  children: [] //Components Here
};

let component = {
  type: "", //Component type,
  style: {}, //style here,
  content: {}, //content here depending on component type,
  stream: {}, //Social Media stream settings
  children: [] //More children
};


<div class="">
    <p>Content</p>
    <div>
        Stream content here
    </div>
</div>