//Dependencies
const express = require("express");
const app = express();
let http = require("http").Server(app);
let port = 3005;
const history = require("connect-history-api-fallback");
var path = require("path");

var bodyParser = require("body-parser");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var mongoose = require("mongoose");
var credentials = require("./config.js");
mongoose.Promise = require("bluebird");
mongoose
  .connect(credentials.mongoConnection, {
    promiseLibrary: require("bluebird"),
    useNewUrlParser: true,
    auth: { authdb: "admin" },
    useFindAndModify: false
  })
  .then(() => console.log("connection succesful"))
  .catch(err => console.error(err));
mongoose.set("useCreateIndex", true);

// app.use(express.static("public"));
// app.use(express.static("dist"));

var auth = require("./routes/auth");
var site = require("./routes/site");
var connect = require("./routes/connect");
var subscribe = require("./routes/subscribe");

app.use("/api/auth", auth);
app.use("/api/site", site);
app.use("/api/connect", connect);
app.use("/api/subscribe", subscribe);

var scrape = require("./routes/scrape");
app.use("/api/scrape", scrape);

app.use(history());
app.use(express.static(__dirname + "/../dist"));

app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname + "/../dist/spa-mat/index.html"));
});

//Initiate
http.listen(port, function() {
  console.log("listening on *:", port);
});
