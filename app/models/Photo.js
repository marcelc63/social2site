var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var MySchema = new Schema({
  parent: {
    type: String
  }
});

module.exports = mongoose.model("Photo", MySchema);
